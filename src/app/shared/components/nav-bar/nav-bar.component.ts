import { Component, OnInit } from '@angular/core';
import { MenuI } from '../../utils/menuItems';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  public menuList: MenuI[] = [
    {
      label: 'Personajes', url: '/personajes', img: 'https://proyecto-dragon-ball.s3.eu-west-3.amazonaws.com/personajes/personajes.jpg'
    },
    {
      label: 'Planetas', url: '/planetas', img: 'https://proyecto-dragon-ball.s3.eu-west-3.amazonaws.com/planetas/planetas.jpg'
    },
    {
      label: 'Videoteca', url: '/videoteca', img: 'https://proyecto-dragon-ball.s3.eu-west-3.amazonaws.com/videoteca/videoteca.jpg'
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
