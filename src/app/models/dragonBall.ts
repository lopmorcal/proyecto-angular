export interface DragonBallI {
    characters: CharactersI[],
    planets: PlanetsI,
    video_library: VideoLibraryI
}

export interface CharactersI {
    id: string,
    name: string,
    race: string,
    gender: string,
    type: string,
    bio: string,
    abilities: string[],
    img: string
}


export interface PlanetsI {
    name: string,
    info: string,
    image: string
}

export interface VideoLibraryI {
    id: string,
    name: string,
    image: string,
    url: string,
    description: string
}
export interface WarriorsI {
    id?: number,
    name: string,
    age: string,
    race: string,
    email: string,
    pass: string,
    repPass: string,
}