import { Component, OnInit } from '@angular/core';
import { MenuI } from 'src/app/shared/utils/menuItems';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public menuList: MenuI[] = [
    {
      label: 'personajes', url: '/personajes', img: 'https://proyecto-dragon-ball.s3.eu-west-3.amazonaws.com/personajes/personajes.jpg'
    },
    {
      label: 'planetas', url: '/planetas', img: 'https://proyecto-dragon-ball.s3.eu-west-3.amazonaws.com/planetas/planetas.jpg'
    },
    {
      label: 'videoteca', url: '/videoteca', img: 'https://proyecto-dragon-ball.s3.eu-west-3.amazonaws.com/videoteca/videoteca.jpg'
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}

