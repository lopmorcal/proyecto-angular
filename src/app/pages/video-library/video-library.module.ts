//modulos
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoLibraryRoutingModule } from './video-library-routing.module';
import { HttpClientModule } from '@angular/common/http';
//Componentes
import { VideoLibraryComponent } from './video-library.component';
import { SharedModule } from 'src/app/shared/shared.module';
//servicios
import { VideosService } from './video-library.service';


@NgModule({
  declarations: [
    VideoLibraryComponent
  ],
  imports: [
    CommonModule,
    VideoLibraryRoutingModule,
    SharedModule,
    HttpClientModule,
  ],
  providers:[VideosService],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class VideoLibraryModule { }
