import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { VideosService } from './video-library.service';
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-video-library',
  templateUrl: './video-library.component.html',
  styleUrls: ['./video-library.component.scss']
})
export class VideoLibraryComponent implements OnInit {

  public videosList : any = []
  private videosSubscription$?:Subscription;
  public videoEmbed: any | null;



  constructor(private allVideosSrv: VideosService,
              private _sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.printAllVideos();
  }

  printAllVideos(){
    let i = 1;
    this.videosSubscription$ = this.allVideosSrv.getAllVideos().subscribe((data)=>{
      this.videosList= data;
    })
  }

  getVideoIframe(videoUrl: string) {
    let video, results;

    if (videoUrl === null) {
      return '';
    }
    results = videoUrl?.match('[\\?&]v=([^&#]*)');
    video = (results === null) ? videoUrl : results[1];

     this.videoEmbed = this._sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + video);
     return this.videoEmbed;
    }
}
