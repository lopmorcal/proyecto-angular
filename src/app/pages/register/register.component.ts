import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WarriorsI } from 'src/app/models/dragonBall';
import { comparePass } from './customvalidator';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  userForm:FormGroup= new FormGroup({});
  subbmited:boolean= false

  constructor(private formbuilder: FormBuilder,
              private registroSrv: RegisterService) {

  }

  ngOnInit(): void {
    this.userForm=this.formbuilder.group(
      {
        name:["",[Validators.required,Validators.minLength(3),Validators.maxLength(30)]],
        age:["",[Validators.required]],
        race:["",[Validators.required]],
        email:["",[Validators.required]],
        pass:["",[Validators.required,Validators.minLength(8),Validators.maxLength(15)]],
        repPass:["",[Validators.required,Validators.minLength(8),Validators.maxLength(15)]],
      },
      {validator: comparePass('pass','repPass')}
    )
  }

  onSubmit(){
    this.subbmited=true;
    window.alert('te has registrado correctamente')
    if (this.userForm.valid) {
      const UserFormRegister: WarriorsI = {
        name: this.userForm.get('name')?.value,
        age: this.userForm.get('age')?.value,
        race: this.userForm.get('race')?.value,
        email: this.userForm.get('email')?.value,
        pass: this.userForm.get('pass')?.value,
        repPass: this.userForm.get('repPass')?.value,
    }
          //AÑADIR EL USUARIO
          this.registroSrv.addwarrior(UserFormRegister).subscribe((datos) => console.log(datos))

    this.userForm.reset();

    this.subbmited=false;

  }
  }
}
