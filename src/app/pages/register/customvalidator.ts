import { FormGroup } from "@angular/forms";

export function comparePass(controlPass: string, controlRepPass:string){
  return (FormGroup:FormGroup)=>{

    const control = FormGroup.controls[controlRepPass];
    const matchingControl= FormGroup.controls[controlRepPass]
    if (matchingControl.errors&& matchingControl.errors['mustMatch']) {
    return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({mustMatch : true})
    } else {
      matchingControl.setErrors(null)
    }

  }
}
