import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvilCharacterComponent } from './evil-character.component';

describe('EvilCharacterComponent', () => {
  let component: EvilCharacterComponent;
  let fixture: ComponentFixture<EvilCharacterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvilCharacterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvilCharacterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
