import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CharactersService } from './characters.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  isLoading:boolean=false
  isOnMode:boolean=true
  showDetailsCharacters:boolean=false
  public charactersList: any = []
  private charactersSubscription$?: Subscription;
  public charactersHeroes: any = []
  public charactersVillano: any = []
  public heroes: any

  constructor(private allCharactersSrv: CharactersService) { }

  ngOnInit(): void {
    this.printAllCharacters();
  }

  printAllCharacters() {
    this.isLoading=true
    this.charactersSubscription$ = this.allCharactersSrv.getAllCharacters().subscribe((datos) => {
      this.charactersList = datos;
      this.isLoading=false
    })
  }

  btnHeroes() {
    this.charactersHeroes=[];
    this.charactersVillano=[]
    for (let item of this.charactersList) {
          if(item.type === 'Heroe') {
            this.charactersHeroes.push(item)
          }}
this.isOnMode=false
this.showDetailsCharacters=true
}

btnVillanos() {
  this.charactersVillano=[]
  this.charactersHeroes=[];
  for (let item of this.charactersList) {
    if(item.type === 'Villano') {
      this.charactersVillano.push(item)
    }}
this.isOnMode=false
this.showDetailsCharacters=true
}

  ngOnDestroy(): void {
    this.charactersSubscription$?.unsubscribe();
  }
}
