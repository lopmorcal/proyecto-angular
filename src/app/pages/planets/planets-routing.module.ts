import { PlanetDetailComponent } from './planet-detail/planet-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlanetsComponent } from './planets.component';


const routes: Routes = [
  { path: '', component: PlanetsComponent },
  { path: ':planetId', component: PlanetDetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanetsRoutingModule { }
